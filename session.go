package main

import (
	"crypto/rand"
	"encoding/base64"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/alexedwards/scs/stores/memstore"
)

var (
	sessionManager = memstore.New(time.Hour * 24 * 7)
)

func CreateNewSession(user_id int64) (string, error) {

	// Generate random string as token id
	token, err := GenerateToken(36)
	if err != nil {
		return "", err
	}
	// Store user id as session data
	data := []byte(strconv.FormatInt(user_id, 10))

	// Set expire date for session to 24h
	expiry := time.Now().Add(time.Hour * 24)

	// Save session
	err = sessionManager.Save(token, data, expiry)
	if err != nil {
		return "", err
	}

	// Return session token
	return token, nil
}

func GetCurrentSession(r *http.Request) (int64, error) {

	// Read token from request header
	token := r.Header.Get("Token")
	if len(token) == 0 {
		return 0, errors.New("Token header is missing")
	}

	// Read session data from memstore
	b, exists, err := sessionManager.Find(token)
	if !exists || err != nil {
		return 0, errors.New("Session not found")
	}

	// Convert data
	user_id, err := strconv.ParseInt(string(b[:]), 10, 64)
	if err != nil {
		return 0, errors.New("Session data error")
	}

	return user_id, nil
}

func RemoveSession() error {

	return errors.New("Error removing session not implemented")
}

func GenerateToken(n int) (string, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)

	return base64.URLEncoding.EncodeToString(b), err
}
