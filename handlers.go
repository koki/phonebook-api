package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

var (
	WelcomeMessage = "Welcome to phonebook service, nothing to see here"
)

func indexPage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, WelcomeMessage)
}

func loginUser(w http.ResponseWriter, r *http.Request) {

	// Read body
	logReq := &LoginRequest{}
	if err := readBodyJSON(r, logReq); err != nil {
		fmt.Println("[[Login user]]:", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Authenticate user, will throw an error if auth data do not match
	user_id, err := authUser(logReq)
	if err != nil {
		fmt.Println("[[Login user]]:", err)
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	// Create new session
	token, err := CreateNewSession(user_id)
	if err != nil {
		fmt.Println("[[Create new session]]:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Write response with generated token
	resp := &TokenResponse{
		Token: token,
	}
	writeJSONResponse(w, resp, http.StatusCreated) // ???? why 201 status here
}

func registerUser(w http.ResponseWriter, r *http.Request) {
	// Read body
	logReq := &LoginRequest{}
	if err := readBodyJSON(r, logReq); err != nil {
		fmt.Println("[[Register user]]:", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err := registerDBUser(logReq)
	if err != nil {
		fmt.Println("[[Register user]]:", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	writeJSONResponse(w, map[string]interface{}{}, http.StatusCreated)
}

func listContacts(w http.ResponseWriter, r *http.Request) {
	// Read session from request
	user_id, err := GetCurrentSession(r)
	if err != nil {
		fmt.Println("[[List Contacts]]:", err)
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	// Get contact list from DB
	contact_list, err := getDBContactsByUserId(user_id)
	if err != nil {
		fmt.Println("[[List Contacts]]:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Write the response
	writeJSONResponse(w, contact_list, http.StatusOK)
}

func createContact(w http.ResponseWriter, r *http.Request) {
	// Read session from request
	user_id, err := GetCurrentSession(r)
	if err != nil {
		fmt.Println("[[Create Contact]]:", err)
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	// Read body
	conReq := &ContactRequest{}
	if err := readBodyJSON(r, conReq); err != nil {
		fmt.Println("[[Create Contact]]:", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Check for empty fileds
	if len(conReq.FirstName) == 0 && len(conReq.LastName == 0) {
		fmt.Println("[[Create Contact]]: Both params 0 len")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Get contacts from DB
	contact_id, err := createDBContact(user_id, conReq)
	if err != nil {
		fmt.Println("[[Create Contact]]:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Write the response
	resp := &ContactResponse{
		Id: contact_id,
	}
	writeJSONResponse(w, resp, http.StatusCreated)
}

func updateContactsName(w http.ResponseWriter, r *http.Request) {
	// Read session from request
	user_id, err := GetCurrentSession(r)
	if err != nil {
		fmt.Println("[[Update Contact Name]]:", err)
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	// Read body
	conReq := &ContactRequest{}
	if err := readBodyJSON(r, conReq); err != nil {
		fmt.Println("[[Update Contact Name]]:", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Check for empty fileds
	if len(conReq.FirstName) == 0 && len(conReq.LastName == 0) {
		fmt.Println("[[Update Contact Name]]: Both params 0 len")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Get contact id
	vars := mux.Vars(r)
	contact_id_str, ok := vars["contacts_id"]
	if !ok {
		fmt.Println("[[Update Contact Name]]:", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	contact_id, _ := strconv.ParseInt(contact_id_str, 10, 64)

	// Update Contact in DB
	if err := updateDBContact(user_id, contact_id, conReq); err != nil {
		fmt.Println("[[Update Contact Name]]:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Write the response
	resp := &ContactResponse{
		Id: contact_id,
	}
	writeJSONResponse(w, resp, http.StatusOK)
}

func deleteContacts(w http.ResponseWriter, r *http.Request) {
	// Read session from request
	user_id, err := GetCurrentSession(r)
	if err != nil {
		fmt.Println("[[Delete Contact]]:", err)
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	// Get contact id
	vars := mux.Vars(r)
	contact_id_str, ok := vars["contacts_id"]
	if !ok {
		fmt.Println("[[Update Contact Name]]:", errors.New("Invalid request url (missing params)"))
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	contact_id, _ := strconv.ParseInt(contact_id_str, 10, 64)

	// Update Contact in DB
	if err := deleteDBContact(user_id, contact_id); err != nil {
		fmt.Println("[[Delete Contact]]:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	writeJSONResponse(w, map[string]interface{}{}, http.StatusOK)
}

func addNumberToContacts(w http.ResponseWriter, r *http.Request) {
	// Read session from request
	user_id, err := GetCurrentSession(r)
	if err != nil {
		fmt.Println("[[Add Entry]]:", err)
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	// Read body
	entReq := &EntryRequest{}
	if err := readBodyJSON(r, entReq); err != nil {
		fmt.Println("[[Add Entry]]:", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Get contact id
	vars := mux.Vars(r)
	contact_id_str, ok := vars["contacts_id"]
	if !ok {
		fmt.Println("[[Add Entry]]:", errors.New("Invalid request url (missing params)"))
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	contact_id, _ := strconv.ParseInt(contact_id_str, 10, 64)

	// Add entry to Contact in DB
	if err := addDBContactEntry(user_id, contact_id, entReq); err != nil {
		fmt.Println("[[Add Entry]]:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Write the response
	resp := &ContactResponse{
		Id: contact_id,
	}
	writeJSONResponse(w, resp, http.StatusCreated)
}

//Helper functions
func readBodyJSON(r *http.Request, dest interface{}) error {

	body, err := ioutil.ReadAll(r.Body)
	r.Body.Close()

	if err != nil {
		return err
	}
	if len(body) == 0 {
		return fmt.Errorf("Error reading body")
	}
	return json.Unmarshal(body, dest)
}

func writeJSONResponse(w http.ResponseWriter, data interface{}, status int) {
	js, err := json.Marshal(data)
	if err != nil {
		fmt.Println("[[writeJSONResponse]]:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(js)
}
