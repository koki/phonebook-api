package main

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type ContactRequest struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

type EntryRequest struct {
	Phone string `json:"phone"`
}

type TokenResponse struct {
	Token string `json:"token"`
}

type ContactResponse struct {
	Id int64 `json:"id"`
}

var router = mux.NewRouter()

func startServer() error {
	router.HandleFunc("/", indexPage)

	router.HandleFunc("/user", loginUser).Methods("POST")
	router.HandleFunc("/register", registerUser).Methods("POST")
	router.HandleFunc("/contacts", listContacts).Methods("GET")
	router.HandleFunc("/contacts", createContact).Methods("POST")
	router.HandleFunc("/contacts/{contacts_id}", updateContactsName).Methods("POST")
	router.HandleFunc("/contacts/{contacts_id}", deleteContacts).Methods("DELETE")
	router.HandleFunc("/contacts/{contacts_id}/entries", addNumberToContacts).Methods("POST")

	http.Handle("/", router)

	port := strconv.Itoa(StartParams.Port)

	fmt.Println("Starting server at", port)

	return http.ListenAndServe(":"+port, nil)
}
