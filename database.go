package main

import (
	"errors"
	"fmt"
	"time"

	"database/sql"

	_ "github.com/go-sql-driver/mysql"
	"golang.org/x/crypto/bcrypt"
)

var Database *sql.DB

type Contact struct {
	Id        int64    `json:"id"`
	FirstName string   `json:"first_name"`
	LastName  string   `json:"last_name"`
	Phones    []string `json:"phones"`
}

type Contacts []*Contact

func initDatabase() error {
	connstr := StartParams.DBdataSourceName

	if len(connstr) == 0 {
		if !StartParams.Production {
			//connstr = "koste:SbHUiSKO7lKdN8A3jiG4ao5WWDWLbm@/phonebook"
			connstr = "kosteblu_test:ytI5-cKuQSg2@tcp(kosteblu.heliohost.org)/kosteblu_phonebook"
		} else {
			return errors.New("Missing valid dbstr start parameter")
		}

	}

	var err error
	Database, err = sql.Open("mysql", connstr)
	if err != nil {
		fmt.Println("Database initialize failed.", err.Error())
		return err
	}

	if err = Database.Ping(); err != nil {
		fmt.Println("Pinging database failed: %s", err.Error())
		return err
	}

	// Create tables

	_, err = Database.Exec("CREATE TABLE IF NOT EXISTS `user` (" +
		"`id` INT NOT NULL AUTO_INCREMENT, " +
		"`username` varchar(100) NOT NULL, " +
		"`password` varchar(255) NOT NULL, " +
		"`activated` tinyint(1) DEFAULT 1, " +
		"`created` datetime NOT NULL, " +
		"PRIMARY KEY (`id`), UNIQUE KEY `username_UNIQUE` (`username`)" +
		") ENGINE=InnoDB AUTO_INCREMENT=100000 DEFAULT CHARSET=utf8",
	)
	if err != nil {
		fmt.Println("Creating table user failed: ", err.Error())
		return err
	}

	_, err = Database.Exec("CREATE TABLE IF NOT EXISTS `contact` (" +
		"`id` INT NOT NULL AUTO_INCREMENT, " +
		"`user_id` INT NOT NULL, " +
		"`first_name` varchar(100) DEFAULT '', " +
		"`last_name` varchar(100) DEFAULT '', " +
		"`created` datetime NOT NULL, " +
		"`modified` datetime NOT NULL, " +
		"PRIMARY KEY (`id`), " +
		"INDEX `par_ind` (`user_id`), " +
		"CONSTRAINT `fk_user` FOREIGN KEY (`user_id`) " +
		"REFERENCES user(`id`) " +
		"ON DELETE CASCADE " +
		") ENGINE=InnoDB AUTO_INCREMENT=100000 DEFAULT CHARSET=utf8",
	)
	if err != nil {
		fmt.Println("Creating table contact failed: ", err.Error())
		return err
	}

	_, err = Database.Exec("CREATE TABLE IF NOT EXISTS `entry` (" +
		"`id` INT NOT NULL AUTO_INCREMENT, " +
		"`user_id` INT NOT NULL, " +
		"`contact_id` INT NOT NULL, " +
		"`phone_number` varchar(100) DEFAULT '', " +
		"`created` datetime NOT NULL, " +
		"`modified` datetime NOT NULL, " +
		"PRIMARY KEY (`id`), " +
		"INDEX `par_ind` (`contact_id`), " +
		"CONSTRAINT `fk_contact` FOREIGN KEY (`contact_id`) " +
		"REFERENCES contact(`id`) " +
		"ON DELETE CASCADE " +
		") ENGINE=InnoDB AUTO_INCREMENT=100000 DEFAULT CHARSET=utf8",
	)
	if err != nil {
		fmt.Println("Creating table entry failed: ", err.Error())
		return err
	}

	fmt.Println("DB good to go")

	return nil
}

func authUser(loginRequest *LoginRequest) (int64, error) {

	var user_id int64
	var password_salted string

	row := Database.QueryRow("select id, password from user where username = ?", loginRequest.Username)
	err := row.Scan(&user_id, &password_salted)
	if err != nil {
		return 0, errors.New("Error reading user from DB")
	}

	bHash := []byte(password_salted)
	bPlain := []byte(loginRequest.Password)

	// Check if password matches
	err = bcrypt.CompareHashAndPassword(bHash, bPlain)
	if err != nil {
		// No match !!
		return 0, errors.New("Invailid password")
	}

	return user_id, nil
}

func registerDBUser(req *LoginRequest) error {

	// Apply some user name and password limitations
	// user_names must be at least 1 chars long,
	// passwords must be at least 6 chars long
	if len(req.Username) < 1 || len(req.Password) < 6 {
		fmt.Println("Register data not long enough")
		return errors.New("Register data not long enough")
	}

	// Hash and salt password before storing to DB
	hash, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println("Error hashing password", err)
		return err
	}

	password_salted := string(hash)

	_, err = Database.Exec(
		"insert into user (username, password, created) values (?, ?, ?)",
		req.Username, password_salted, time.Now().UTC(),
	)

	return err
}

func deleteDBUser(user_id int64) error {

	_, err := Database.Exec("delete from user where id = ?", user_id)
	if err != nil {
		fmt.Println("Error delete DB user", err)
		return err
	}
	return nil
}

func getDBContactsByUserId(user_id int64) (*Contacts, error) {
	contacts := Contacts{}

	// Create contacts map
	map_contacts := make(map[int64]*Contact)

	// Query all user contacts
	cont_rows, err := Database.Query("select id, first_name, last_name from contact where user_id = ?", user_id)
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
		}

		return &contacts, err
	}

	// Store contacts to map first
	for cont_rows.Next() {
		var c Contact
		err = cont_rows.Scan(&c.Id, &c.FirstName, &c.LastName)
		if err != nil {
			continue
		}
		c.Phones = []string{}
		map_contacts[c.Id] = &c
	}

	// Query all user entries
	ent_rows, err := Database.Query("select contact_id, phone_number from entry where user_id = ?", user_id)
	if err == nil {
		for ent_rows.Next() {
			var contact_id int64
			var phone_number string

			err = ent_rows.Scan(&contact_id, &phone_number)
			if err != nil {
				continue
			}

			// Append to the contacts they belong
			if contact, ok := map_contacts[contact_id]; ok {
				map_contacts[contact_id].Phones = append(contact.Phones, phone_number)
			}
		}
	}

	// Generate contacts array from map (friendly to json.Marshal)
	for _, contact := range map_contacts {
		contacts = append(contacts, contact)
	}

	return &contacts, nil
}

func createDBContact(user_id int64, req *ContactRequest) (int64, error) {
	result, err := Database.Exec(
		"insert into contact (user_id, first_name, last_name, created, modified) values (?, ?, ?, ?, ?)",
		user_id, req.FirstName, req.LastName, time.Now().UTC(), time.Now().UTC(),
	)

	if err != nil {
		fmt.Println("Error insert DB contact", err)
		return 0, err
	}

	return result.LastInsertId()
}

func updateDBContact(user_id int64, contact_id int64, req *ContactRequest) error {
	fName := req.FirstName
	lName := req.LastName

	_, err := Database.Exec("update contact set first_name = ?, last_name = ?, modified = ? where user_id = ? and id = ?",
		fName, lName, time.Now().UTC(), user_id, contact_id,
	)

	if err != nil {
		fmt.Println("Error update DB contact", err)
		return err
	}

	return nil
}

func deleteDBContact(user_id int64, contact_id int64) error {

	_, err := Database.Exec("delete from contact where user_id = ? and id = ?", user_id, contact_id)
	if err != nil {
		fmt.Println("Error delete DB contact", err)
		return err
	}
	return nil
}

func addDBContactEntry(user_id int64, contact_id int64, req *EntryRequest) error {
	phone := req.Phone

	_, err := Database.Exec(
		"insert into entry (user_id, contact_id, phone_number, created, modified) values (?, ?, ?, ?, ?)",
		user_id, contact_id, phone, time.Now().UTC(), time.Now().UTC(),
	)

	if err != nil {
		fmt.Println("Error insert DB entry", err)
		return err
	}

	return nil
}
