# Phonebook API service #

To build project you will need
* [go](https://golang.org/)


## Build steps
```go get```

```go build```

## Testing
```go test```



## Running

To run the project you will need working MySQL database as well


Run the following command

```phonebook -port=[defaults to 8080] -dbstr=user:pass@/database -production``` to start the service

When running locally (while developing) you don't need any start parameters to run it, however passing a -dbstr might speed things up,
since MySQL hosting provided as a backup is extremly slow.



## API

* /register (POST)
* /user (POST)
* /contacts (GET)
* /conatcts (POST)
* /contacts/[[conatcts_id]] (POST)
* /contacts/[[conatcts_id]] (DELETE)
* /contacts/[[conatcts_id]]/entries (POST)