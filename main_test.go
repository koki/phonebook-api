package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	//"github.com/stretchr/testify/assert"
)

func checkError(err error, t *testing.T) {
	if err != nil {
		t.Errorf("An error occurred. %v", err)
	}
}

func TestHomePage(t *testing.T) {

	req, err := http.NewRequest("GET", "/", nil)

	checkError(err, t)

	rr := httptest.NewRecorder()

	//Make the handler function satisfy http.Handler
	http.HandlerFunc(indexPage).ServeHTTP(rr, req)

	//Confirm the response has the right status code
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Home status code differs. Expected %d .\n Got %d instead", http.StatusOK, status)
	}

	if rr.Body.String() != WelcomeMessage {
		t.Errorf("Home response body differs. Expected: %s .\n Got: %s instead", WelcomeMessage, rr.Body.String())
	}
}

// Write tests for all API calls ...
