package main

import (
	"flag"
	"fmt"
)

type startParams struct {
	Production       bool
	Port             int
	DBdataSourceName string
}

var StartParams = &startParams{}

func init() {

	// Read start parameters
	flag.BoolVar(&StartParams.Production, "production", false, "Service is running in production mode")
	flag.IntVar(&StartParams.Port, "port", 8080, "Port to use [ defaults to 8080]")
	flag.StringVar(&StartParams.DBdataSourceName, "dbstr", "", "Data source name")

	flag.Parse()

	fmt.Println("Start parameters successfully read")
}

func main() {

	// Init database, make sure it works before starting the server
	if err := initDatabase(); err != nil {
		fmt.Println("Database error, make sure you have the DB running: ", err)
		return
	}

	fmt.Println("Server: ", startServer())
}
